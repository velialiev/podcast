import React, { FC } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { NavigationScreenProp } from 'react-navigation'
import theme from '../../configs/theme'
import Button from '../../components/Button'

const PodcastsScreen: FC<Props> = ({ navigation }) => {
  return (
    <View style={{ ...theme.defaultScreenStyles, ...styles.screenWrap }}>
      <View style={styles.contentWrap}>
        <Image source={require('../../assets/startLogo.png')} style={styles.logo} />
        <View style={styles.textWrap}>
          <Text style={styles.title}>Добавьте первый подкаст</Text>
          <Text style={styles.text}>Добавляйте, редактируйте и делитесь</Text>
          <Text style={styles.text}>подкастами вашего сообщества.</Text>
        </View>

        <View>
          <Button onPress={() => navigation.navigate('PodcastScreen')}>Добавить подкаст</Button>
        </View>
      </View>
    </View>
  )
}

interface Props {
  navigation: NavigationScreenProp<any, any>
}

const styles = StyleSheet.create({
  screenWrap: {
    paddingHorizontal: theme.screenHorizontalPadding,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentWrap: {
    marginBottom: theme.titleBarHeight,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textWrap: {
    marginBottom: 24,
  },
  text: {
    textAlign: 'center',
    color: theme.mediumGrayColor,
    fontSize: 16,
  },
  title: {
    color: theme.darkColor,
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 10,
  },
  logo: {
    marginBottom: 10,
  },
})

export default PodcastsScreen
