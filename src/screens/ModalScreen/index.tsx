import React, { useState } from 'react'
import { View } from 'react-native'

const ModalScreen = ({ route, navigation }: any) => {
  const [isClosed, setIsClosed] = useState(false)
  const { renderContent } = route.params
  const close = () => {
    if (isClosed) {
      return
    }

    setIsClosed(true)
    navigation.goBack()
  }

  return (
    <View style={{ flex: 1, backgroundColor: '#fff', paddingVertical: 12 }}>
      {renderContent({ close })}
    </View>
  )
}

export default ModalScreen
