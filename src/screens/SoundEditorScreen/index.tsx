import React, { FC, RefObject, useCallback, useEffect, useRef, useState } from 'react'
import {
  EventEmitter,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableNativeFeedback,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import theme from '../../configs/theme'
import { NavigationScreenProp } from 'react-navigation'
import { RouteProp } from '@react-navigation/native'
import { FilePickerFile } from 'react-native-file-picker'
import Trimmer from '../../components/Trimmer'
import { MediaInformation } from 'react-native-ffmpeg'
import Sound from 'react-native-sound'
import Input from '../../components/Input'
import { FieldArray, Formik, FormikProps } from 'formik'
import Button from '../../components/Button'

// Sound.setCategory('Playback', true)

const playIcon = require('../../assets/icons/play.png')
const pauseIcon = require('../../assets/icons/pause_28.png')
const chartIcon = require('../../assets/icons/chart.png')
const musicIcon = require('../../assets/icons/music.png')
const scissorsIcon = require('../../assets/icons/scissors.png')
const backIcon = require('../../assets/icons/back.png')
const reversedChart = require('../../assets/icons/reversedChart.png')
const addCircleIcon = require('../../assets/icons/add_circle_24.png')
const removeCircleIcon = require('../../assets/icons/remove_circle_24.png')

const generateRandomId = () => Math.floor(Math.random() * 10 ** 10)

const SoundEditorScreen: FC<Props> = ({ navigation, route }) => {
  const { uploadedFile, mediaInformation, timings } = route.params as RouteParams
  const [scrubberPosition, setScrubberPosition] = useState(0)
  const [leftHandlePosition, setLeftHandlePosition] = useState(0)
  const [rightHandlePosition, setRightHandlePosition] = useState(20000)
  const [currentSound, setCurrentSound] = useState<Sound>()
  const [isPausedManually, setIsPausedManually] = useState(true)
  const [showTrimmer, setShowTrimmer] = useState(false)
  const [init, setInit] = useState(true)

  const ref = useRef<RefObject<FormikProps<{ timings: Timing[] }>>>(null)

  // RNFFmpeg.executeWithArguments(['-i', uploadedFile.path])
  //   .then(console.log
  //   )
  const handleRewind = async (scrubberPosition: number) => {
    const sound = currentSound || ((await playSound()) as Sound)

    if (!sound.isPlaying()) {
      resumeIfPausedAutomatically()
    }

    setScrubberPosition(scrubberPosition)
    sound.setCurrentTime(scrubberPosition / 1000)
  }

  const playSound = useCallback(() => {
    return new Promise((resolve) => {
      const sound = new Sound(uploadedFile.path, Sound.CACHES, (e) => {
        const interval = setInterval(() => {
          sound.getCurrentTime((currentTime) => {
            const updatedScrubberPosition = currentTime * 1000
            setScrubberPosition(updatedScrubberPosition)
          })
        }, 50)

        if (!init) {
          sound.play(() => {
            clearInterval(interval)
            setCurrentSound(undefined)
          })
        }

        setInit(false)

        resolve(sound)
      })

      setCurrentSound(sound)
    })
  }, [uploadedFile.path])

  const resumeIfPausedAutomatically = () => {
    if (!currentSound || currentSound.isPlaying() || isPausedManually) {
      return
    }

    currentSound.play()
  }

  useEffect(() => {
    return () => currentSound?.stop()
  }, [currentSound])

  useEffect(() => {
    if (!currentSound) {
      playSound()
      return
    }

    if (isPausedManually) {
      currentSound.pause()
    } else {
      currentSound.play()
    }
  }, [isPausedManually, currentSound, uploadedFile, playSound])

  useEffect(() => {
    if (!showTrimmer) {
      setLeftHandlePosition(scrubberPosition)
      setRightHandlePosition(scrubberPosition + 20000)
    }
  }, [scrubberPosition, showTrimmer])

  return (
    <ScrollView style={theme.defaultScreenStyles}>
      <>
        <Trimmer
          showTrimmer={showTrimmer}
          minimumTrimDuration={2500}
          maxTrimDuration={1000000}
          zoomMultiplier={1}
          initialZoomValue={1}
          trackBorderColor="#d7d8d9"
          scrubberColor="#FF3347"
          scrubberPosition={scrubberPosition}
          markerColor="#3F8AE0"
          trackBackgroundColor="#F2F3F5"
          tintColor={theme.lightBlueColor}
          totalDuration={mediaInformation?.duration || 1}
          trimmerLeftHandlePosition={leftHandlePosition}
          trimmerRightHandlePosition={rightHandlePosition}
          onScrubbingComplete={(scrubberPosition) => {
            handleRewind(scrubberPosition)
          }}
          onHandleChange={({ leftPosition, rightPosition }) => {
            setLeftHandlePosition(leftPosition)
            setRightHandlePosition(rightPosition)

            if (leftPosition > scrubberPosition) {
              handleRewind(leftPosition)
            }

            if (scrubberPosition < rightPosition) {
              resumeIfPausedAutomatically()
            }
          }}
          onTrimmerEnd={() => showTrimmer && currentSound?.pause()}
        />

        <Button
          onPress={() => setShowTrimmer(!showTrimmer)}
          type="secondary"
          style={{ width: '100%', marginTop: 15, marginBottom: 15 }}
        >
          {showTrimmer ? 'Перейти в режим прослушивания' : 'Перейти в режим редактирования'}
        </Button>

        <View style={styles.buttons}>
          <View style={styles.buttonGroup}>
            <TouchableOpacity
              onPress={() => setIsPausedManually(!isPausedManually)}
              style={{
                ...styles.squareButton,
                backgroundColor: theme.lightBlueColor,
                marginLeft: 0,
              }}
            >
              <Image
                source={isPausedManually ? playIcon : pauseIcon}
                style={styles.squareButtonIcon}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.buttonGroup}>
            <TouchableOpacity style={styles.squareButton}>
              <Image source={scissorsIcon} style={styles.squareButtonIcon}/>
            </TouchableOpacity>

            <TouchableOpacity style={styles.squareButton}>
              <Image source={backIcon} style={styles.squareButtonIcon}/>
            </TouchableOpacity>
          </View>

          <View style={styles.buttonGroup}>
            <TouchableOpacity style={styles.squareButton}>
              <Image source={musicIcon} style={styles.squareButtonIcon}/>
            </TouchableOpacity>

            <TouchableOpacity style={styles.squareButton}>
              <Image source={chartIcon} style={styles.squareButtonIcon}/>
            </TouchableOpacity>

            <TouchableOpacity style={styles.squareButton}>
              <Image source={reversedChart} style={styles.squareButtonIcon}/>
            </TouchableOpacity>
          </View>
        </View>
      </>

      <Formik
        innerRef={ref as any}
        initialValues={{
          timings: timings || [],
        }}
        onSubmit={(values) => {
          navigation.navigate('PodcastScreen', values)
        }}
      >
        {({ values, handleSubmit }) => (
          <FieldArray
            name="timings"
            render={(arrayHelpers) => (
              <View>
                <Text style={styles.title}>Таймкоды</Text>

                <FlatList
                  data={values.timings}
                  renderItem={(timing) => (
                    <View style={styles.timing} key={timing.item.id}>
                      <TouchableOpacity onPress={() => arrayHelpers.remove(timing.index)}>
                        <Image style={styles.removeCircleIcon} source={removeCircleIcon}/>
                      </TouchableOpacity>

                      <Input
                        placeholder="Описание"
                        style={styles.timingDescriptionInput}
                        name={`timings[${timing.index}].description`}
                      />
                      <Input
                        placeholder="Время"
                        style={styles.timingTimeInput}
                        mask="[00]:[00]"
                        name={`timings[${timing.index}].time`}
                      />
                    </View>
                  )}
                />

                <TouchableNativeFeedback
                  onPress={() => arrayHelpers.push({ id: generateRandomId() })}
                >
                  <View style={styles.addTimeCodeButton}>
                    <Image style={styles.addTimeCodeButtonIcon} source={addCircleIcon}/>
                    <Text style={styles.addTimeCodeButtonLabel}>Добавить таймкод</Text>
                  </View>
                </TouchableNativeFeedback>

                <Text style={styles.hint}>
                  Отметки времени с названием темы. Позволяют слушателям легче путешествовать по
                  подкасту.
                </Text>

                <Button
                  onPress={handleSubmit}
                  style={{ height: 44, width: '100%', marginTop: 20, marginBottom: 30 }}
                >
                  Применить
                </Button>
              </View>
            )}
          />
        )}
      </Formik>
    </ScrollView>
  )
}

interface Props {
  navigation: NavigationScreenProp<any, any>
  route: RouteProp<any, any>
}

interface RouteParams {
  uploadedFile: FilePickerFile
  mediaInformation: MediaInformation
  timings: Timing[]
}

const styles = StyleSheet.create({
  trimmerCard: {
    borderRadius: 10,
    borderColor: '#99A2AD',
    borderWidth: 0.5,
  },
  squareButton: {
    backgroundColor: '#F2F3F5',
    width: 44,
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginLeft: 6,
  },
  squareButtonIcon: {
    width: 20,
    height: 20,
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 8,
  },
  buttonGroup: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  hint: {
    color: theme.mediumGrayColor,
    fontSize: 13,
    marginTop: 4,
  },
  title: {
    textTransform: 'uppercase',
    fontSize: 13,
    color: theme.mediumGrayColor,
    fontWeight: theme.semiBoldFontWeight,
    marginTop: 26,
    marginBottom: 18,
  },
  addTimeCodeButton: {
    paddingVertical: 12,
    flexDirection: 'row',
    alignItems: 'center',
  },
  addTimeCodeButtonIcon: {
    width: 22,
    height: 22,
    marginRight: 12,
  },
  addTimeCodeButtonLabel: {
    color: theme.lightBlueColor,
    fontSize: 16,
  },
  timing: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 24,
  },
  removeCircleIcon: {
    width: 24,
    height: 24,
  },
  timingDescriptionInput: {
    width: '65%',
  },
  timingTimeInput: {
    width: '20%',
  },
})

export interface Timing {
  id: number
  description: string
  time: string
}

export default SoundEditorScreen
