import React, { FC, useEffect, useState } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import theme from '../../configs/theme'
import { NavigationScreenProp } from 'react-navigation'
import Button from '../../components/Button'
import PlayerButtons from '../../components/PlayerButtons'
import { RouteProp } from '@react-navigation/native'
import FilePickerManager, { FilePickerFile, FilePickerResult } from 'react-native-file-picker'
import { MediaInformation, RNFFprobe } from 'react-native-ffmpeg'
import convertMillisecondsToTime from '../../utils/convertMillisecondsToTimeParts'
import ImageUploader from '../../components/ImageUploader'
import { Formik } from 'formik'
import Input from '../../components/Input'
import FormCheckbox from '../../components/FormCheckbox'
import Select from '../../components/Select'
import { Timing } from '../SoundEditorScreen'

const accessibility = [
  {
    id: 'all',
    type: 'Всем пользователям',
    description: 'При публикации записи с эпизодом, он становится доступным для всех пользователей',
  },
  {
    id: 'me',
    type: 'Только мне',
    description: 'При публикации записи с эпизодом, он становится доступным только для вас',
  },
]

const audioIcon = require('../../assets/icons/audio.png')

const PodcastScreen: FC<Props> = ({ navigation, route }) => {
  const [uploadedFile, setUploadedFile] = useState<FilePickerFile>()
  const [mediaInformation, setMediaInformation] = useState<MediaInformation>()
  const [duration, setDuration] = useState('')
  const { timings } = (route.params || {}) as {
    timings: Timing[]
  }

  console.log(route.params)

  useEffect(() => {
    if (!uploadedFile) {
      return
    }

    RNFFprobe.getMediaInformation(uploadedFile.path).then((info) => {
      setMediaInformation(info)

      if (info.duration) {
        setDuration(convertMillisecondsToTime(info.duration))
      }
    })
  }, [uploadedFile])

  const uploadPodcast = () => {
    FilePickerManager.showFilePicker((res) => {
      setUploadedFile(res as FilePickerFile)
    })
  }

  return (
    <Formik
      initialValues={{
        avatar: '',
        name: '',
        description: '',
        adultContent: false,
        removeEpisodeFromExport: false,
        trailer: false,
        accessibility: 'all',
      }}
      onSubmit={(values) => {
        console.log(values)
      }}
    >
      {({ values }) => (
        <View style={{ ...theme.defaultScreenStyles }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.nameAndImageRow}>
              <ImageUploader name="avatar" style={styles.image} />
              <Input
                style={styles.input}
                name="name"
                label="Название"
                placeholder="Введите название подкаста"
              />
            </View>

            <Input
              textAlignVertical="top"
              numberOfLines={3}
              multiline={true}
              name="description"
              label="Описание подкаста"
              placeholder="Введите описание подкаста"
            />

            {!uploadedFile ? (
              <View style={styles.fileUploadSection}>
                <View style={styles.textWrap}>
                  <Text style={styles.title}>Загрузите Ваш подкаст</Text>
                  <Text style={styles.text}>Выберите готовый аудиофайл из вашего</Text>
                  <Text style={styles.text}>телефона и добавьте его</Text>
                </View>

                <View>
                  <Button type="secondary" onPress={uploadPodcast}>
                    Загрузить файл
                  </Button>
                </View>
              </View>
            ) : (
              <View style={styles.podcastWithHint}>
                <View style={styles.podcast}>
                  <View style={styles.nameAndAudioIconWrap}>
                    <View style={styles.audioIconWrap}>
                      <Image style={styles.icon} source={audioIcon} />
                    </View>
                    <Text style={styles.audioName}>{uploadedFile.fileName}</Text>
                  </View>
                  <Text style={styles.audioDuration}>{duration}</Text>
                </View>
                <Text style={styles.hint}>
                  Вы можете добавить таймкоды и скорректировать подкаст в режиме редактирования
                </Text>

                <Button
                  style={{ width: '100%' }}
                  type="secondary"
                  onPress={() =>
                    navigation.navigate('SoundEditorScreen', {
                      uploadedFile,
                      mediaInformation,
                      timings,
                    })
                  }
                >
                  Редактировать аудиозапись
                </Button>
              </View>
            )}

            <View style={styles.checkboxes}>
              <FormCheckbox name="adultContent">Ненормативный контент</FormCheckbox>
              <FormCheckbox name="removeEpisodeFromExport">
                Исключить эпизод из экспорта
              </FormCheckbox>
              <FormCheckbox name="trailer">Трейлер подкаста</FormCheckbox>
            </View>

            <Select
              name="accessibility"
              data={accessibility}
              label="Кому доступен данный подкаст"
              resolveId={(item) => item.id}
              resolveTitle={(item) => item.type}
              resolveDescription={(item) => item.description}
              modalTitle="Кому доступен данный подкаст"
              navigation={navigation}
            />

            <Button
              onPress={() =>
                navigation.navigate('PodcastDescriptionsScreen', {
                  ...values,
                  timings,
                  duration,
                })
              }
              style={styles.primaryButton}
            >
              Продолжить
            </Button>
          </ScrollView>
        </View>
      )}
    </Formik>
  )
}

interface Props {
  navigation: NavigationScreenProp<any, any>
  route: RouteProp<any, any>
}

const styles = StyleSheet.create({
  primaryButton: {
    width: '100%',
    paddingVertical: 11.5,
    marginBottom: 12,
    marginTop: 36,
  },
  image: {
    marginRight: 12,
  },
  input: {
    width: '86%',
  },
  nameAndImageRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 26,
  },
  text: {
    textAlign: 'center',
    color: theme.mediumGrayColor,
    fontSize: 16,
  },
  title: {
    color: theme.darkColor,
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 10,
  },
  textWrap: {
    marginBottom: 24,
  },
  fileUploadSection: {
    marginTop: 44,
    marginBottom: 32,
    justifyContent: 'center',
    alignItems: 'center',
  },
  checkboxes: {
    marginBottom: 26,
  },
  podcastWithHint: {
    marginTop: 32,
    marginBottom: 22,
  },
  podcast: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  nameAndAudioIconWrap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  audioIconWrap: {
    marginRight: 12,
    backgroundColor: '#F2F3F5',
    justifyContent: 'center',
    alignItems: 'center',
    width: 48,
    height: 48,
    borderRadius: 10,
  },
  icon: {
    width: 24,
    height: 24,
  },
  audioName: {
    fontSize: 16,
  },
  audioDuration: {
    color: '#99A2AD',
    fontSize: 13,
  },
  hint: {
    color: theme.mediumGrayColor,
    fontSize: 13,
    marginTop: 10,
    marginBottom: 18,
  },
})

export default PodcastScreen
