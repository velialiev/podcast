import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import theme from '../../configs/theme'
import Button from '../../components/Button'

const DoneScreen = () => {
  return (
    <View style={{ ...theme.defaultScreenStyles, ...styles.screenWrap }}>
      <View style={styles.contentWrap}>
        <Image source={require('../../assets/icons/done.png')} style={styles.logo} />
        <View style={styles.textWrap}>
          <Text style={styles.title}>Подкаст добавлен</Text>
          <Text style={styles.text}>Раскажите своим подписчикам</Text>
          <Text style={styles.text}>о новом подкасте, чтобы получить</Text>
          <Text style={styles.text}>больше слушателей.</Text>
        </View>

        <View>
          <Button>Поделиться подкастом</Button>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  screenWrap: {
    paddingHorizontal: theme.screenHorizontalPadding,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentWrap: {
    marginBottom: theme.titleBarHeight,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textWrap: {
    marginBottom: 24,
  },
  text: {
    textAlign: 'center',
    color: theme.mediumGrayColor,
    fontSize: 16,
  },
  title: {
    color: theme.darkColor,
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 10,
  },
  logo: {
    marginBottom: 10,
  },
})

export default DoneScreen
