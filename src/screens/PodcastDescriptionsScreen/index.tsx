import React, { FC } from 'react'
import { View, Text, StyleSheet, Image, CheckBox, FlatList } from 'react-native'
import theme from '../../configs/theme'
import Input from '../../components/Input'
import { Formik } from 'formik'
import Select from '../../components/Select'
import { NavigationScreenProp } from 'react-navigation'
import Button from '../../components/Button'
import { Timing } from '../SoundEditorScreen'
import { RouteProp } from '@react-navigation/native'

const selectData = [{ id: 0, text: 'Всем пользователям' }]

const backgroundIcon = require('../../assets/icons/backgroundIcon.png')

const PodcastsDescriptionsScreen: FC<Props> = ({ navigation, route }) => {
  const data = route.params as RouteData
  console.log(data)
  console.log(data)

  return (
    <View style={{ ...theme.defaultScreenStyles }}>
      <View>
        <View style={styles.header}>
          <View style={styles.wrap}>
            {data.avatar ? (
              <Image width={72} height={72} style={styles.preview} source={{ uri: data.avatar }} />
            ) : (
              <Image width={25} height={25} style={styles.icon} source={backgroundIcon} />
            )}
          </View>
          <View style={styles.headerText}>
            <Text style={styles.title}>{data.name}</Text>
            <Text style={styles.author}>ПараDogs</Text>
            <Text style={styles.duration}>Длительность: {data.duration}</Text>
          </View>
        </View>

        <View style={styles.delimiter} />

        <View>
          <Text style={styles.subtitle}>Описание</Text>
          <Text style={styles.text}>{data.description}</Text>
        </View>

        <View style={styles.delimiter} />

        <View>
          <Text style={styles.subtitle}>Содержание</Text>
          <FlatList
            data={data.timings}
            renderItem={({ item }) => (
              <Text
                style={{
                  ...styles.text,
                  ...styles.timing,
                }}
              >
                <Text style={styles.timecode}>{item.time}</Text> - {item.description}
              </Text>
            )}
          />
        </View>

        <View style={styles.delimiter} />

        <Button
          onPress={() => navigation.navigate('DoneScreen')}
          style={{ height: 44, width: '100%', marginTop: 20, marginBottom: 30 }}
        >
          Опубликовать подкаст
        </Button>
      </View>
    </View>
  )
}

export default PodcastsDescriptionsScreen

const styles = StyleSheet.create({
  image: {
    width: 72,
    height: 72,
    borderRadius: 8,
    marginRight: 14,
  },
  header: {
    flexDirection: 'row',
  },
  headerText: {
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  title: {
    fontSize: 19,
    fontWeight: theme.semiBoldFontWeight,
  },
  author: {
    fontSize: 14,
    color: '#4986CC',
  },
  duration: {
    fontSize: 14,
    color: theme.mediumGrayColor,
  },
  subtitle: {
    paddingBottom: 12,
    fontSize: 19,
    fontWeight: theme.semiBoldFontWeight,
  },
  text: {
    fontSize: 17,
  },
  timing: {
    paddingVertical: 14,
  },
  delimiter: {
    backgroundColor: '#D7D8D9',
    height: 0.5,
    marginVertical: 18,
  },
  timecode: {
    color: '#4986CC',
  },
  icon: {
    width: 25,
    height: 25,
  },
  preview: {
    width: 80,
    height: 80,
    borderRadius: 10,
  },
  wrap: {
    width: 82,
    height: 82,
    backgroundColor: '#F2F3F5',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: '#d5d6d8',
    marginRight: 12,
  },
})

interface Props {
  navigation: NavigationScreenProp<any, any>
  route: RouteProp<any, any>
}

interface RouteData {
  avatar: string
  name: string
  description: string
  adultContent: boolean
  removeEpisodeFromExport: boolean
  trailer: boolean
  accessibility: string
  timings: Timing[]
  duration: string
}
