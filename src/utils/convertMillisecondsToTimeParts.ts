const convertMillisecondsToTime = (ms: number) => {
  let seconds = Math.floor((ms / 1000) % 60)
  let minutes = Math.floor((ms / (1000 * 60)) % 60)
  let hours = Math.floor((ms / (1000 * 60 * 60)) % 24)

  hours = hours < 10 ? +('0' + hours) : hours
  minutes = minutes < 10 ? +('0' + minutes) : minutes
  seconds = seconds < 10 ? +('0' + seconds) : seconds

  let result = ''

  if (hours) {
    result += hours
  }

  if (minutes) {
    result += minutes + ':'
  }

  if (seconds) {
    result += seconds
  }

  return result
}

export default convertMillisecondsToTime
