import 'react-native-gesture-handler'
import React, { useState } from 'react'
import Navigation from './Navigation'

const App = () => {
  return <Navigation />
}

export default App
