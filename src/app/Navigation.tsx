import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import ModalScreen from '../screens/ModalScreen'
import PodcastsScreen from '../screens/PodcastsScreen'
import { NavigationContainer } from '@react-navigation/native'
import PodcastScreen from '../screens/PodcastScreen'
import TargetDonationScreen from '../screens/SoundEditorScreen'
import PodcastDescriptionsScreen from '../screens/PodcastDescriptionsScreen'
import DoneScreen from '../screens/DoneScreen'

const PodcastsStack = createStackNavigator()

const MainStackComponent = () => {
  return (
    <PodcastsStack.Navigator initialRouteName="DonationsScreen">
      <PodcastsStack.Screen
        name="PodcastsScreen"
        options={{ title: 'Новый подкаст' }}
        component={PodcastsScreen}
      />

      <PodcastsStack.Screen
        name="PodcastScreen"
        options={{ title: 'Новый подкаст' }}
        component={PodcastScreen}
      />

      <PodcastsStack.Screen
        name="SoundEditorScreen"
        options={{ title: 'Редактор звука' }}
        component={TargetDonationScreen}
      />

      <PodcastsStack.Screen
        name="PodcastDescriptionsScreen"
        options={{ title: 'Новый подкаст' }}
        component={PodcastDescriptionsScreen}
      />

      <PodcastsStack.Screen
        name="DoneScreen"
        options={{ title: '', headerShown: false }}
        component={DoneScreen}
      />

      <PodcastsStack.Screen
        name="ModalScreen"
        component={ModalScreen}
        options={({ route }: any) => ({
          title: route.params?.headerTitle,
          headerBackTitle: route.params?.headerBackTitle,
        })}
      />
    </PodcastsStack.Navigator>
  )
}

const Navigation = () => {
  return (
    <NavigationContainer>
      <MainStackComponent />
    </NavigationContainer>
  )
}

export default Navigation
