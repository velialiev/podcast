import React, { FC, ReactElement } from 'react'
import { StyleSheet, Text, TextInput, TextInputProps, View, ViewStyle } from 'react-native'
import theme from '../../configs/theme'
import { useField } from 'formik'
// @ts-ignore
import TextInputMask from 'react-native-text-input-mask';

const Input: FC<Props> = ({
  name,
  label,
  mask,
  renderValue = (value) => value,
  suffix,
  ...props
}) => {
  const [field, , helper] = useField(name)

  const resolveInputStyles = () => {
    const inputStyles: ViewStyle = { ...styles.input }

    if (suffix) {
      inputStyles.paddingRight = 44
    }

    return inputStyles
  }

  const TextField = mask ? TextInputMask : TextInput

  return (
    <View style={props.style}>
      {label && <Text style={styles.label}>{label}</Text>}
      <TextField
        {...props}
        mask={mask}
        value={renderValue(field.value)}
        onChange={(e: any) => helper.setValue(e.nativeEvent.text)}
        placeholderTextColor={theme.mediumGrayColor}
        style={{
          ...(props.style as any),
          ...resolveInputStyles(),
        }}
      />
      {suffix && <View style={styles.suffixWrap}>{suffix}</View>}
    </View>
  )
}

interface Props extends TextInputProps {
  name: string
  renderValue?: (value: string) => string
  mask?: string
  label?: string
  suffix?: ReactElement
}

const styles = StyleSheet.create({
  label: {
    marginBottom: 8,
    fontSize: 14,
    color: theme.grayColor,
  },
  input: {
    width: '100%',
    fontSize: 16,
    borderRadius: theme.borderRadius,
    color: theme.darkColor,
    borderWidth: 0.5,
    borderColor: '#d5d6d8',
    borderStyle: 'solid',
    backgroundColor: '#f2f3f5',
    padding: 12,
  },
  suffixWrap: {
    width: 44,
    height: 44,
    position: 'absolute',
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default Input
