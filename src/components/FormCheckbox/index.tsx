import React, { FC } from 'react'
import {
  CheckBoxProps,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback, TouchableNativeFeedback,
} from 'react-native'
import { useField } from 'formik'
import CheckBox from '@react-native-community/checkbox'
import theme from '../../configs/theme'

const FormCheckbox: FC<Props> = ({ name, children, ...props }) => {
  const [field, , handler] = useField(name)

  return (
    <TouchableNativeFeedback onPress={() => handler.setValue(!field.value)}>
      <View style={styles.checkboxWrap}>
        <CheckBox
          {...props}
          tintColors={{
            true: theme.lightBlueColor,
            false: '#B8C1CC',
          }}
          value={field.value}
          onChange={(event: any) => handler.setValue(event.nativeEvent.value)}
          style={styles.checkbox}
        />

        <Text style={styles.label}>{children}</Text>
      </View>
    </TouchableNativeFeedback>
  )
}

interface Props extends CheckBoxProps {
  name: string
}

const styles = StyleSheet.create({
  checkboxWrap: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    paddingVertical: 6,
  },
  checkbox: {
    marginRight: 6,
  },
  label: {
    fontSize: 17,
  },
})

export default FormCheckbox
