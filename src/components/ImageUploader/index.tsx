import React, { FC, useState } from 'react'
import { StyleSheet, View, Image, Text, TouchableOpacity, ViewStyle } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import theme from '../../configs/theme'
import { ImagePickerResponse } from 'react-native-image-picker/src/internal/types'
import { useField } from 'formik'

const backgroundIcon = require('../../assets/icons/backgroundIcon.png')

const imageUploaderOptions = {
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
  allowsEditing: true,
  chooseFromLibraryButtonTitle: 'Выбрать из галерии',
  takePhotoButtonTitle: 'Сделать фотографию',
  cancelButtonTitle: 'Отмена',
  mediaType: 'photo' as MediaType,
}

type MediaType = 'photo' | 'video' | 'mixed'

const ImageUploader: FC<Props> = ({ name, label = 'Загрузить изображение', style }) => {
  const [field, , helper] = useField(name)

  const chooseImage = () => {
    ImagePicker.showImagePicker(
      {
        ...imageUploaderOptions,
        title: label,
      },
      (res) => {
        if (!res.uri) {
          return
        }

        helper.setValue(res.uri)
      },
    )
  }

  const removePreview = () => {
    helper.setValue(undefined)
  }

  return (
    <TouchableOpacity style={{ ...styles.wrap, ...style }} onPress={chooseImage}>
      {field.value ? (
        <Image width={72} height={72} style={styles.preview} source={{ uri: field.value }} />
      ) : (
        <Image width={25} height={25} style={styles.icon} source={backgroundIcon} />
      )}
    </TouchableOpacity>
  )
}

interface Props {
  name: string
  label?: string
  style?: ViewStyle
}

const styles = StyleSheet.create({
  wrap: {
    width: 82,
    height: 82,
    backgroundColor: '#F2F3F5',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: '#d5d6d8',
  },
  icon: {
    width: 25,
    height: 25,
  },
  preview: {
    width: 80,
    height: 80,
    borderRadius: 10,
  },
})

export default ImageUploader
