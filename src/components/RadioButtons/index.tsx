import React, { PropsWithChildren } from 'react'
import { StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import theme from '../../configs/theme'

const RadioButtons = <T,>({
  label,
  data,
  onChange,
  resolveId,
  resolveRadioLabel,
  checkedId,
}: PropsWithChildren<Props<T>>) => {
  const resolveRadioStyles = (item: T) => {
    const radioStyles = { ...styles.radio }

    if (resolveId(item) === checkedId) {
      Object.assign(radioStyles, styles.activeRadio)
    }

    return radioStyles
  }

  return (
    <View>
      {label && <Text style={styles.label}>{label}</Text>}
      {data.map((item) => (
        <TouchableWithoutFeedback onPress={() => onChange(item)} key={resolveId(item)}>
          <View style={styles.buttonWrap}>
            <View style={resolveRadioStyles(item)}>
              {resolveId(item) === checkedId && <View style={styles.checkedMark} />}
            </View>
            <Text style={styles.radioLabel}>{resolveRadioLabel(item)}</Text>
          </View>
        </TouchableWithoutFeedback>
      ))}
    </View>
  )
}

interface Props<T> {
  label?: string
  data: T[]
  onChange: (item: T) => void
  resolveId: (item: T) => string | number
  resolveRadioLabel: (item: T) => string
  checkedId?: string | number
}

const styles = StyleSheet.create({
  label: {
    marginBottom: 8,
    fontSize: 14,
    color: theme.grayColor,
  },
  buttonWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 11,
  },
  radio: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 22,
    height: 22,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: theme.lightGrayColor,
    marginRight: 13,
  },
  activeRadio: {
    borderColor: theme.lightBlueColor,
  },
  radioLabel: {
    fontSize: 16,
  },
  checkedMark: {
    width: 14,
    height: 14,
    backgroundColor: theme.lightBlueColor,
    borderRadius: 100,
  },
})

export default RadioButtons
