import React, { FC, useMemo } from 'react'
import { StyleSheet, TouchableOpacity, Text, TouchableOpacityProps } from 'react-native'
import theme from '../../configs/theme'

const Button: FC<Props> = ({ type = 'primary', children, ...props }) => {
  const style = useMemo(() => {
    const result = {
      ...styles.button,
      ...(props.style as any),
    }

    if (type === 'secondary') {
      Object.assign(result, styles.secondaryButton)
    }

    return result
  }, [props.style, type])

  const textStyle = useMemo(() => {
    const result = {
      ...styles.buttonText,
    }

    if (type === 'secondary') {
      Object.assign(result, styles.secondaryButtonText)
    }

    return result
  }, [type])

  return (
    <TouchableOpacity {...props} style={style}>
      <Text style={textStyle}>{children}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  button: {
    alignSelf: 'flex-start',
    justifyContent: 'center',
    paddingHorizontal: 16,
    paddingVertical: 8,
    backgroundColor: theme.blueColor,
    borderRadius: theme.borderRadius,
  },
  buttonText: {
    fontWeight: theme.mediumFontWeight,
    fontSize: 15,
    color: theme.lightColor,
    textAlign: 'center',
  },
  secondaryButton: {
    borderColor: theme.lightBlueColor,
    borderWidth: 0.5,
    backgroundColor: '#fff',
  },
  secondaryButtonText: {
    color: theme.lightBlueColor,
  },
})

interface Props extends TouchableOpacityProps {
  type?: Type
}

type Type = 'primary' | 'secondary'

export default Button
