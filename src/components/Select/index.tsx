import React, { PropsWithChildren, useMemo } from 'react'
import {
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TouchableHighlight,
  ViewStyle,
} from 'react-native'
import { NavigationScreenProp } from 'react-navigation'
import { useField } from 'formik'
import theme from '../../configs/theme'

const arrowDownIcon = require('../../assets/icons/arrowDownIcon.png')

const arrowRightIconImage = (
  <Image
    width={14}
    height={9.5}
    style={{ width: 14, height: 9.5, transform: [{ rotate: '-90deg' }] }}
    source={arrowDownIcon}
  />
)

const Select = <T,>({
  name,
  label,
  data,
  resolveId,
  resolveTitle,
  resolveDescription,
  modalTitle,
  navigation,
  style,
}: PropsWithChildren<Props<T>>) => {
  const [field, , helper] = useField(name)

  const openModal = () => {
    navigation.navigate('ModalScreen', {
      headerTitle: modalTitle,
      headerBackTitle: 'Назад',
      renderContent: ({ close }: any) => (
        <FlatList
          data={data}
          renderItem={(item) => {
            const i = item.item as T

            return (
              <TouchableHighlight
                underlayColor={'rgba(0, 0, 0, 0.12)'}
                style={styles.item}
                onPress={() => {
                  helper.setValue(resolveId(i))
                  close()
                }}
              >
                <Text style={styles.itemText}>{resolveTitle(i)}</Text>
              </TouchableHighlight>
            )
          }}
        />
      ),
    })
  }

  const values = useMemo(() => {
    const item = data.find((i) => resolveId(i) === field.value)

    if (!item) {
      return {
        title: '',
        description: '',
      }
    }

    return {
      title: resolveTitle(item),
      description: resolveDescription(item),
    }
  }, [data, field.value])

  return (
    <>
      <TouchableOpacity style={style} onPress={openModal}>
        <View style={styles.wrap}>
          {/*<Input*/}
          {/*  renderValue={() => value}*/}
          {/*  name={name}*/}
          {/*  label={label}*/}
          {/*  placeholder={placeholder}*/}
          {/*  suffix={arrowRightIconImage}*/}
          {/*/>*/}
          <View>
            <Text style={styles.label}>{label}</Text>
            <Text style={styles.value}>{values.title}</Text>
          </View>
          {arrowRightIconImage}
        </View>
        <Text style={styles.description}>{values.description}</Text>
      </TouchableOpacity>
    </>
  )
}

interface Props<T> {
  label: string
  placeholder?: string
  data: T[]
  name: string
  modalTitle: string
  resolveId: (item: T) => string | number
  resolveTitle: (item: T) => string
  resolveDescription: (item: T) => string
  navigation: NavigationScreenProp<any, any>
  style?: ViewStyle
}

const styles = StyleSheet.create({
  item: {
    paddingVertical: 11,
    paddingHorizontal: 12,
  },
  itemText: {
    fontSize: 16,
  },
  wrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  label: {
    fontSize: 17,
  },
  value: {
    fontSize: 13,
    color: theme.mediumGrayColor,
  },
  description: {
    marginTop: 8,
    fontSize: 16,
    color: theme.mediumGrayColor,
  },
})

export default Select
